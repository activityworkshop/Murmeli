# Murmeli
An encrypted, serverless friend-to-friend messaging application using PGP encryption and Tor's hidden services.

The tool is very much still under development (and currently temporarily broken in places).  The code published here covers the startup wizard and many components of the Murmeli application itself.  This includes the step-by-step tool to check dependencies, set up the database, connect to tor and create the unique keypair for encryption; also the basic management of the contacts list and sending and receiving messages.  The rest of the code will follow as it becomes more stable, but feedback is welcome on the code published so far.

More information about Murmeli is published online here:
    https://activityworkshop.net/software/murmeli/
including explanations of the concepts and ideas, some screenshots and a pair of youtube videos demonstrating setup, establishing contact and exchanging messages.  Feedback, criticism and review of these proposals are very welcome.

Put briefly, the aim is to produce a way of sending encrypted messages from peer to peer, without using a server.  The communication is done by both sides publishing tor hidden services, and the encryption is done using asymmetric PGP once public keys have been exchanged and verified.  Because it runs without a server, peers have to be online at the same time in order to exchange messages.  However, mutual trusted peers can act as blind relays for the encrypted messages, thereby reducing latency.

It uses Python3 and Qt5 for the desktop application, and it stores the messages inside a local file-based database.  It should be cross-platform, but until now it has only been tested on linux (Debian, Raspbian and Mint) with all dependencies available from the standard (stable) repositories.  Testing on Windows is ongoing.

Please try out the code and report back any difficulties encountered.  The tool can be started with:

        PYTHONPATH=. python3 murmeli/startmurmeli.py

and the tests can be run with (for example):

        PYTHONPATH=. python3 test/systemtest.py

Linting can be done with any tool of course, for example:

        pylint3 murmeli
 
All feedback and help is very welcome.

## Known issues

* In the startup wizard, it would be nice to be able to select the executable paths with a file dialog.
* The messages view doesn't yet have any sorting or paging options, it just shows all messages with the newest first.
* It's not yet clear whether each message has been 'read' or is 'unread'.  Do we need this concept, and if so do we want to have to click each message to mark it as 'read'?
* Due to changes made between Gpg1 and Gpg2, there are new issues with generating a keypair on platforms like the Pi Zero.  The current workaround is to generate the keypair on another machine, and then to move over the <code>keyring</code> directory over to the Pi.</p>
* Since the move from Qt4 to Qt5, and therefore from QtWebKit to QtWebEngine, Murmeli's GUI will no longer run on platforms which don't support these two components (including Chromium).  So no more GUI on the Raspberry Pi, for example.
* In the unfortunate event that Murmeli crashes, the changes to the database which are still held in memory are suddenly lost.  At the very least these should be regularly written to disk to minimise data loss, and even better would be some emergency flushing when an exception is thrown.

Fixed issues
* Using Qt4 there was an intermittent "Segmentation Fault" problem somewhere in the Qt library which proved rather difficult to diagnose.  Fortunately this appears to be gone since the move to Qt5.

## Redesign

Recently the architecture was redesigned to make things more modular and testable.  In particular, it plugs together components at runtime instead of coupling them in a fixed way.  This should get rid of the singletons and make things much more separable and testable.  It then becomes possible to plug together a partial system for tests, and for the robot instances a gui-less system without dependencies on Qt.  The robot can optionally have an LED matrix for a mouth now, too (for example using a Pimoroni Scrollbot).

We'll also focus much more closely on the outputs of pylint and try to name things and indent things in a more pylint-friendly way.

