# German texts for Murmeli

[setup]
qtoquit=(oder 'q' zum Abbrechen)
abort=Setup abgebrochen.
configsaved=Configdatei gespeichert.
languageselected=Deutsche Sprache ausgewählt.
foundgpgversion=GPG gefunden, Version: %s
notfoundgpgversion=Aufruf nach gpg fehlgeschlagen.  Namen / Pfad setzen?
entergpgpath=Aufruf nach 'gpg' fehlgeschlagen.  Namen und Pfad bitte eingeben:
foundkeyring=Schlüsselbund gefunden
nokeyring=Schlüsselbund nicht gefunden
foundkeys=%d private und %d öffentliche Schlüssel gefunden
datadir=Ordner für Murmeli-Daten
createdatadir=Datenordner kreieren?
createdir.create=Kreieren
createddir.cancel=Abbrechen
datadir.creating=Ordner kreiert.
entertorpath=Pfad zum Tor exe (manchmal nur 'tor')
startingtorfailed=Starten von Tor ist fehlgeschlagen.  Pfad überprüfen.
foundtorid=Die neue Murmeli-Id lautet '%s'
genkeypair=Ein neues Schlüsselpaar muss jetzt generiert werden.  Das dauert ja ein bisschen.
genkeypair.rsa=RSA Schlüsselpaar generieren
genkeypair.name=Name (benötigt)
genkeypair.email=Email (optional)
genkeypair.comment=Bemerkung (optional)
genkeypair.pleasewait=Bitte Geduld haben während der Generierung...
genkeypair.complete=Schlüsselpaar generiert.
selectprivatekey=Dieser Schlüsselbund hat mehrere Schlüssel.  Welcher sollte für Murmeli verwendet werden?
realorrobot=Sollte dieses System ein normales sein, oder einen Robotersystem für Weiterleitung?
system.real=Normales System
system.robot=Robotersystem für das Weiterleiten der Nachrichten
selectrobotownerkey=Den Schlüssel des Owners auswählen, entweder vom Schlüsselbund\noder von einer .key Datei im Datenverzeichnis
refreshkeylist=Liste aktualisieren
exportpublickey=Den öffentlichen Schlüssel zur Datei exportieren (für das Aufsetzten eines Robotersystems)
publickeyexported=Um ein Roboter aufzusetzen, die Schlüsseldatei '%s' ins Datenverzeichnis des Roboters kopieren.
yes=Ja
no=Nein

[startupwizard]
title=Murmeli Startup Wizard
intro.heading=Murmeli ist ein Kommunikationsmittel, für dich.  Unter deine Kontrolle.
intro.description1=Mit Murmeli ist alles was du schickst und empfängst verschlüsselt, and auschliesslich nur für dich und deine Freunde lesbar.
intro.description2=Es gibt keinen zentralen Server, es läuft alles nur auf den Rechnern die ihr steuert und vertraut.  Es ist Email, es ist Bloggen, es ist Foto-verteilen, es ist Quatschen.
intro.description3=Zuerst gibt es ein paar Aufstartschritte zu erledigen, und dann kannst du beginnen, dich mit deinen Freunden zu verbinden.
dependencies.heading=Abhängigkeiten
dependencies.intro=Murmeli braucht folgende Programmen und Bibliotheken um zu laufen
dependencies.pyqt=PyQt (und Qt), für Dialoge und Darstellung
dependencies.gnupg=Python-GnuPG (und GPG) für Ver- und Entschlüsselung
dependencies.allfound=Das sieht gut aus, jetzt weiter!
dependencies.notallfound=Murmeli kann nicht ohne diese laufen.  Bitte nochmals prüfen und ggf. installieren.
dependencies.alsotor=Murmeli braucht auch Tor zu kommunizieren, dies wird später geprüft.
dep.found=Gefunden
dep.notfound=Fehlt
paths.heading=Dateipfade
paths.configfile=Konfigurationsdatei
paths.datadir=Data Verzeichnis,<br>wo alle Nachrichten gespeichert werden
paths.torexe=Pfad zum Tor
paths.gpgexe=Pfad zum Gpg
paths.considerencryption=Es könnte sich lohnen, das Data Verzeichnis innerhalb einen<br>verschlüsselten Volume zu platzieren, falls vorhanden.
paths.failedtocreatedatadir=Der Versuch, die Verzeichnisse zu kreieren, ist fehlgeschlagen.<br>Bitte einen anderen Pfad auswählen.
services.heading=Dienstleistungen
services.intro=Murmeli braucht folgende Dienstleistungen initialisiert und gestartet zu sein
services.database=Die lokalen Datenbank
services.gpg=GPG, für die Keyringverwaltung
services.tor=Tor, für die Routing von Nachrichten
services.abouttostart=Diese Dienstleistungen werden jetzt gestartet...
services.allstarted=Alle Dienstleistungen wurden erfolgreich gestartet.
services.notallstarted=Nicht alle Dienstleistungen konnten gestartet werden.  Bitte prüfen.
keygen.heading=Key Generierung
keygen.introemptykeyring=Dein Schlüsselbund ist momentan leer, das heisst es muss ein Schlüsselpaar generiert werden.
keygen.introsinglekey='Weiter' klicken um mit diesem Schlüsselpaar fortzusetzen.
keygen.introselectkey=Bitte einen von den folgenden Schlüsseln auswählen, oder einen neuen generieren.
keygen.param.name=Name oder Spitzname
keygen.param.email=Email (optional)
keygen.param.comment=Bemerkung (optional)
keygen.mighttakeawhile=Die Schlüsselgenerierung könnte ein paar Minuten in Anspruch nehmen.  Bitte Geduld haben.
finished.heading=Aufsetzen abgeschlossen
finished.congrats=Glückwunsch, Murmeli ist jetzt startbereit.
finished.nowstart=Jetzt kannst du Murmeli starten und beginnen, Freunde zu kontaktieren.
finished.yourid=Du kannst ihnen deinen Murmeli-Id geben: <big><code>%s</code></big>.

[mainwindow]
title=Murmeli
toolbar.home=Zuhause
toolbar.contacts=Kontakte
toolbar.messages=Nachrichten
toolbar.calendar=Kalender
toolbar.settings=Einstellungen

[home]
title=Murmeli

[contacts]
title=Kontakte
online=online
onlinesince=seit %s online
offlinesince=seit %s abwesend
adduser.intro=Gib bitte die Id des Benutzers ein, und eine Nachricht um dich vorzustellen.
adduser.checkuserid=Die Benutzerid sollte 16 Character lang und alphanumerisch sein.
adduser.notownuserid=Die Benutzerid sollte nicht identisch sein wie deiner.
adduser.useridfield=Benutzerid (16 Chars)
adduser.displaynamefield=Name der angezeigt wird
adduser.messagefield=Nachricht, um dich vorzustellen
exportkey.intro=Um deinen Roboter aufzusetzen, musst du deinen öffentlichen Schlüssel zuerst exportieren und auf den Roboter kopieren.
exportkey.buttonpreamble=Mit diesem Knopf kannst du den öffentlichen Schlüsseln nach deinem Data-Ordner exportieren
addrobot.intro=Gib bitte die Id des Roboters ein.  Der Roboter musst deinen Schlüssel schon kennen.
details.name=Name
details.displayname=Für mich so anzeigen
details.owndescription=Wer bist du?
details.description=Beschreibung
details.interests=Interessen
details.birthday=Geburtstag
details.userid=Benutzerid
details.robotid=Roboter
details.robotid.none=Keinen gesetzt
details.robotid.requested=Angefordert
details.robotid.enabled=Aktiviert
details.robotid.enabled.online=Aktiviert, online
details.robotid.enabled.offline=Aktiviert, offline
details.sharedcontacts=Gemeinsame Bekannten
details.editlink.before=Diese Details
details.editlink.after=
link.addnewcontact=Kontakt hinzufügen
link.addrobot=Roboter hinzufügen
link.showstorm=Netzwerk skizzieren
storm.title=Murmeli Netzwerk
confirmdelete=Willst du sicher diesen Kontakt löschen?  Ihr werdet nicht mehr miteinander kommunizieren, ohne die Verbindung neu aufzubauen.
desc.confirmfingerprints=Dieser Schlüssel ist noch nicht bestätigt worden.  Um sicherzustellen, dass niemand einen anderen Schlüssel dazwischengefügt hat, musst du dies bestätigen.
link.confirmfingerprints=Schlüssel bestätigen
fingerprintcheck.intro1=Es ist ganz wichtig zu prüfen ob dieser Schlüssel der richtige ist, auch dann wenn du sicher bist, dass die Person stimmt!
fingerprintcheck.intro2=Um das zu machen, brauchst du eine andere Kontaktmöglichkeit, (wie telefonisch, oder persönlich) und gibst der Kontakt die folgenden Wörter.  Das muss unbedingt abseits von Murmeli stattfinden für eine unabhängige Bestätigung.
fingerprintcheck.yourwords=Hier sind die fünf Wörter die du '%s' mitteilen solltest.
fingerprintcheck.theirwords=Und '%s' sollte dir einen von diesen Wortreihen geben.
fingerprintcheck.alreadydone=Du hast die Wörter von '%s' schon bestätigt.
error.fingerprintcheckfailed=Die sind nicht die erwartete Wörter.  Bitte nochmals prüfen.

[messages]
title=Nachrichten
createnew=Neue Nachricht schreiben
search=Nachrichten durchsuchen
searchfor=Suche nach
contactrequests=Kontakt-Anfragen
prompt.contactaccept=Nachricht, um die Anfrage von '%s' zu akzeptieren
prompt.contactreject=Anfrage von '%s' ablehnen
contactrequest.refused=Deine Anfrage ist nicht akzeptiert worden.
contactrequest.accepted=Deine Anfrage wurde akzeptiert.
contactrequest.acceptednomessage=Deine Anfrage wurde akzeptiert, aber ohne Nachricht.
contactrequest.recommends=empfiehlt
contactrequest.requestsrefer=möchte eine Verbindung zu
contactresponses=Kontakt-Antworten
contactresponse.delete=Diese Antwort von '%s' löschen?
contactrefer.confirm=Empfehlungen an '%s' und '%s' schicken
delete=Diese Nachricht löschen?
mails=Mails
from=Von
sendtime=Gesendet
to=An
nomessages=Keine neue Nachrichten.
searchnomessages=Es wurden keine Suchresultaten gefunden.
sender.unknown=Absender unbekannt
recpt.unknown=<unbekannt>
confirm.messagesent=Nachricht geschickt.
sendtime.yesterday=Gestern

[composemessage]
title=Neue Nachricht
to=An
to.helptext=Antippen um die Empfänger auszuwählen
messagegoeshere=Hier die Nachricht
norecipients=Bitte das 'An:' Feld antippen um die Empfänger auszuwählen
nomessagebody=Bitte eine Nachricht eingeben

[calendar]
title=Kalender

[settings]
title=Einstellungen
intro=Hier kannst du die Einstellungen für Sprache und Veröffentlichung setzen:
language=Sprache
friendsseefriends=Kontakte sind für andere sichtbar
allowfriendrequests=Anfragen von Unbekannten erlaubt
showlogwindow=Logmeldungen anzeigen

[button]
back=Zurück
exit=Beenden
next=Weiter
ok=OK
cancel=Abbrechen
finish=Fertig
generate=Generieren
send=Abschicken
change=Ändern
edit=Editieren
bold=Fett
italic=Cursiv
underline=Unterstrichen
accept=Verbinden
reject=Ablehnen
delete=Löschen
reply=Beantworten
addall=+ Alle
removeall=- Alle
search=Suchen
recommend=Empfehlen
export=Exportieren

[gui]
dialogtitle.error=Fehler
dialogtitle.openimage=Bilddatei Öffnen
fileselection.filetypes.jpg=Bilddateien (*.jpg)

[warning]
keysnotfoundfor=Schlüsselbund hat keine Schlüssel für:
