'''Module for utils for contact lists'''


def get_ids(contacts):
    '''Return a set of tor ids from the given contact list'''
    ids = set()
    if contacts:
        for entry in contacts.split(","):
            if len(entry) > 16:
                ids.add(entry[:16])
    return ids

def get_ids_and_names(contacts):
    '''Return a set of tuples from the given contact list'''
    pairs = set()
    if contacts:
        for entry in contacts.split(","):
            if len(entry) > 16:
                pairs.add((entry[:16], entry[16:]))
    return pairs
