'''Message handlers for Murmeli'''

from murmeli.system import System, Component
from murmeli.config import Config
from murmeli.contactmgr import ContactManager
from murmeli.contactcheck import ContactChecker
from murmeli import message
from murmeli import dbutils
from murmeli import inbox
from murmeli import pendingtable


class MessageHandler(Component):
    '''Abstract message handler'''

    def __init__(self, parent):
        Component.__init__(self, parent, System.COMPNAME_MSG_HANDLER)

    def receive(self, msg):
        '''Receive an incoming message'''
        if msg and isinstance(msg, message.Message):
            print("*** Message type is :", msg.msg_type)
            # Check if it's from a trusted sender
            if msg.sender_must_be_trusted:
                # Check sender
                if not self.is_from_trusted_contact(msg):
                    print("Ignoring message from untrusted contact:",
                          msg.get_field(msg.FIELD_SENDER_ID))
                    return
            if msg.msg_type == msg.TYPE_CONTACT_REQUEST:
                self.receive_contact_request(msg)
            elif msg.msg_type == msg.TYPE_CONTACT_RESPONSE:
                self.receive_contact_response(msg)
            elif msg.msg_type == msg.TYPE_STATUS_NOTIFY:
                self.receive_status_notify(msg)
            elif msg.msg_type == msg.TYPE_INFO_REQUEST:
                self.receive_info_request(msg)
            elif msg.msg_type == msg.TYPE_INFO_RESPONSE:
                self.receive_info_response(msg)
            elif msg.msg_type == msg.TYPE_FRIEND_REFERRAL:
                self.receive_friend_referral(msg)
            elif msg.msg_type == msg.TYPE_FRIENDREFER_REQUEST:
                self.receive_friend_refer_request(msg)
            elif msg.msg_type == msg.TYPE_REGULAR_MESSAGE:
                self.receive_regular_message(msg)
            elif msg.msg_type == msg.TYPE_RELAYED_MESSAGE:
                self.receive_relayed_message(msg)

    def receive_contact_request(self, msg):
        '''Receive a contact request'''
        pass

    def receive_contact_response(self, msg):
        '''Receive a contact response'''
        pass

    def receive_status_notify(self, msg):
        '''Receive a status notification'''
        if isinstance(msg, message.StatusNotifyMessage):
            print("MessageHandler received a StatusNotify from:",
                  msg.get_field(msg.FIELD_SENDER_ID))
        # TODO: Does a robot need to send pong replies at all?
        # If it's a ping, reply with a pong
        if msg.get_field(msg.FIELD_PING) and msg.get_field(msg.FIELD_ONLINE):
            if not self.is_from_trusted_contact(msg):
                return
            # Create new pong for the sender and pass to outbox
            sender_of_ping = msg.get_field(msg.FIELD_SENDER_ID)
            pong = self._create_pong(sender_of_ping)
            dbutils.add_message_to_outbox(pong, self.get_component(System.COMPNAME_CRYPTO),
                                          self.get_component(System.COMPNAME_DATABASE))

    def _create_pong(self, recipient):
        '''Create a StatusNotify pong message for the given recipient'''
        pong = message.StatusNotifyMessage()
        pong.set_field(pong.FIELD_PING, 0)
        pong.recipients = [recipient]
        # Calculate profile hash and add to msg
        database = self.get_component(System.COMPNAME_DATABASE)
        own_hash = dbutils.calculate_hash(database.get_profile())
        pong.set_field(pong.FIELD_PROFILE_HASH, own_hash)
        return pong

    def receive_info_request(self, msg):
        '''Receive an info request'''
        pass
    def receive_info_response(self, msg):
        '''Receive an info response'''
        pass
    def receive_friend_referral(self, msg):
        '''Receive a friend referral'''
        pass
    def receive_friend_refer_request(self, msg):
        '''Receive a friend referral request'''
        pass
    def receive_regular_message(self, msg):
        '''Receive a regular message'''
        pass

    def receive_relayed_message(self, msg):
        '''Receive a relayed message for somebody else'''
        sender_id = msg.get_field(msg.FIELD_SENDER_ID) if msg else None
        database = self.get_component(System.COMPNAME_DATABASE)
        dbutils.add_relayed_message_to_outbox(msg, sender_id, database)

    def is_from_trusted_contact(self, msg):
        '''Return true if given message is from a contact with trusted status'''
        return self._get_sender_status(msg) in ['trusted', 'robot']

    def is_from_known_contact(self, msg):
        '''Return true if given message is from either trusted or untrusted contact'''
        return self._get_sender_status(msg) in ['trusted', 'untrusted', 'robot']

    def _get_sender_status(self, msg):
        '''Return status of the sender of the given message from the database'''
        sender_id = msg.get_field(msg.FIELD_SENDER_ID) if msg else None
        return self._get_contact_status(sender_id)

    def _get_contact_status(self, tor_id):
        '''Return status of the sender of the given message from the database'''
        if tor_id:
            profile = self.call_component(System.COMPNAME_DATABASE, "get_profile", torid=tor_id)
            if profile:
                return profile.get('status')
        return None

    def _send_info_response(self, req_msg):
        '''Send an info response to the given info request message'''
        if req_msg.get_field(req_msg.FIELD_INFOTYPE) == req_msg.INFO_PROFILE:
            sender_id = req_msg.get_sender_id()
            print("Should send profile info to:", sender_id)
            outmsg = message.InfoResponseMessage()
            database = self.get_component(System.COMPNAME_DATABASE)
            own_profile = database.get_profile() if database else {}
            own_hash = dbutils.calculate_hash(own_profile)
            own_profile['profileHash'] = own_hash
            profile_string = dbutils.get_profile_as_string(own_profile)
            outmsg.set_field(outmsg.FIELD_RESULT, profile_string)
            outmsg.recipients = [sender_id]
            dbutils.add_message_to_outbox(outmsg, self.get_component(System.COMPNAME_CRYPTO),
                                          database)
            print("Sent an InfoResponse")


class RobotMessageHandler(MessageHandler):
    '''Message handler subclass for robot system'''

    def __init__(self, parent):
        MessageHandler.__init__(self, parent)

    def is_from_trusted_contact(self, msg):
        '''Return true if given message is from a contact with trusted status'''
        return self._get_sender_status(msg) in ['trusted', 'owner']

    def receive_contact_request(self, msg):
        '''Receive a contact request'''
        print("Robot message handler receives a contact request")
        # Compare incoming keyid with owner id in Config, ignore all others
        sender_keyid = msg.get_field(msg.FIELD_SENDER_KEY)
        owner_keyid = self.get_config_property(Config.KEY_ROBOT_OWNER_KEY)
        print("Contact request was from '%s', my owner is '%s'" % (sender_keyid, owner_keyid))
        if sender_keyid == owner_keyid and sender_keyid:
            sender_id = msg.get_field(msg.FIELD_SENDER_ID)
            print("Contact request from my owner with id '%s'" % sender_id)
            # Check if owner already set, if so then don't change it
            database = self.get_component(System.COMPNAME_DATABASE)
            if not database:
                return
            crypto = self.get_component(System.COMPNAME_CRYPTO)
            if not database.get_profiles_with_status(status="owner"):
                # update db with SENDER_ID
                owner_profile = {"torid":sender_id, 'status':'owner', 'keyid':owner_keyid}
                # NOTE: For conreq to robot, only the keyid is sent, not the whole key
                database.add_or_update_profile(profile=owner_profile)
            # Send an automatic accept message
            resp = message.ContactAcceptMessage()
            resp.recipients = [sender_id]
            own_profile = database.get_profile()
            resp.set_field(resp.FIELD_MESSAGE, "I'm your robot")
            resp.set_field(resp.FIELD_SENDER_NAME, "Robot")
            own_keyid = own_profile.get('keyid')
            own_publickey = crypto.get_public_key(own_keyid) if crypto else None
            resp.set_field(resp.FIELD_SENDER_KEY, own_publickey)
            resp.set_field(resp.FIELD_SENDER_ID, own_profile.get('torid'))
            dbutils.add_message_to_outbox(resp, crypto, database)
        else:
            print("Contact request wasn't from our owner so I'll ignore it")


    def receive_friend_referral(self, msg):
        '''Receive a friend referral'''
        if self._is_message_from_owner(msg):
            # Get referred friend out of message
            new_friend_id = msg.get_field(msg.FIELD_FRIEND_ID)
            print("Friend referral is for my owner's friend '%s'" % new_friend_id)
            new_friend_key = msg.get_field(msg.FIELD_FRIEND_KEY)
            # Add key to keyring and get keyid
            friend_keyid = self.call_component(System.COMPNAME_CRYPTO, "import_public_key",
                                               strkey=new_friend_key)
            if friend_keyid:
                # Update database
                profile = {'status':'trusted', 'keyid':friend_keyid}
                database = self.get_component(System.COMPNAME_DATABASE)
                dbutils.create_profile(database, new_friend_id, profile)

    def _is_message_from_owner(self, msg):
        '''Return true if message is from this robot's owner'''
        sender_id = msg.get_field(msg.FIELD_SENDER_ID)
        owner_profiles = self.call_component(System.COMPNAME_DATABASE,
                                             "get_profiles_with_status",
                                             status="owner")
        owner_profile = owner_profiles[0] if owner_profiles else None
        owner_id = owner_profile.get('torid') if owner_profile else None
        return owner_id and sender_id == owner_id

    def receive_info_request(self, msg):
        '''Receive an info request'''
        print("Received info request")
        if self._is_message_from_owner(msg):
            print("Info request is from my owner so I'll answer it")
            self._send_info_response(msg)


class RegularMessageHandler(MessageHandler):
    '''Message handler subclass for regular (human-based) system'''

    def __init__(self, parent):
        MessageHandler.__init__(self, parent)

    def receive_status_notify(self, msg):
        '''Receive a status notification'''
        # Reply to a ping with a pong
        MessageHandler.receive_status_notify(self, msg)
        # Update our list of who is online, offline
        sender_id = msg.get_field(msg.FIELD_SENDER_ID)
        is_online = msg.get_field(msg.FIELD_ONLINE)
        self.call_component(System.COMPNAME_CONTACTS, "set_online_status", tor_id=sender_id,
                            online=is_online)
        # Compare profile hash with stored one
        received_hash = msg.get_field(msg.FIELD_PROFILE_HASH)
        if received_hash:
            print("Got hash: '%s'" % received_hash)
            database = self.get_component(System.COMPNAME_DATABASE)
            profile = self.call_component(System.COMPNAME_DATABASE, "get_profile", torid=sender_id)
            if profile and profile.get('status') in ['trusted', 'robot']:
                if profile.get('profileHash') != received_hash:
                    print("Profile hash is different, need to send an InfoRequest")
                    inforeq = message.InfoRequestMessage()
                    inforeq.recipients = [sender_id]
                    dbutils.add_message_to_outbox(inforeq,
                                                  self.get_component(System.COMPNAME_CRYPTO),
                                                  database)
                    print("Added an InfoRequest to outbox")
                else:
                    print("Profile hash already stored:", received_hash)
            else:
                print("Couldn't find trusted profile for '%s'" % sender_id)

    def receive_contact_request(self, msg):
        '''Receive a contact request, store in inbox (or just throw it away)'''
        # Maybe: check we haven't got this contact already, or its status if we have
        # Check config to see whether we accept untrusted contact requests
        if self.call_component(System.COMPNAME_CONFIG, "get_property",
                               key=Config.KEY_ALLOW_FRIEND_REQUESTS):
            dbutils.add_message_to_inbox(msg, self.get_component(System.COMPNAME_DATABASE),
                                         inbox.MC_CONREQ_INCOMING)

    def receive_contact_response(self, msg):
        '''Receive a contact response (either accept or refuse)'''
        # TODO: Check if this is a response from our robot, if so then treat differently
        sender_id = msg.get_field(msg.FIELD_SENDER_ID)
        database = self.get_component(System.COMPNAME_DATABASE)
        if isinstance(msg, message.ContactDenyMessage):
            print("  MessageHandler process Deny from '%s'" % sender_id)
            ContactManager(database, None).handle_receive_deny(sender_id)
            dbutils.add_message_to_inbox(msg, database, inbox.MC_CONRESP_REFUSAL)
        elif isinstance(msg, message.ContactAcceptMessage):
            print("  MessageHandler process Accept from '%s'" % sender_id)
            sender_status = dbutils.get_status(database, sender_id)
            if sender_status in ['pending', 'requested', 'reqrobot', 'untrusted']:
                crypto = self.get_component(System.COMPNAME_CRYPTO)
                from_robot = ContactManager(database, crypto).is_robot_id(sender_id)
                if from_robot:
                    # We have a new robot, so need to inform friends using referral messages
                    ContactChecker(database, crypto).check_robot_connections()
                else:
                    # Only add message to inbox if it's not from the robot
                    dbutils.add_message_to_inbox(msg, database, inbox.MC_CONRESP_ACCEPT)
            elif sender_status in [None, 'blocked', 'deleted']:
                print("Received a contact response but I didn't send them a request!")
                database.add_row_to_pending_table(pendingtable.create_row(msg))
        else:
            assert False

    def receive_info_request(self, msg):
        '''Receive an info request'''
        print("RegularMessageHandler, Info request received!")
        sender_id = msg.get_sender_id()
        print("Info request is from sender:", sender_id)
        if dbutils.is_trusted(self.get_component(System.COMPNAME_DATABASE), sender_id):
            print("Info request is from trusted sender, so should reply with info")
            self._send_info_response(msg)

    def receive_info_response(self, msg):
        '''Receive an info response'''
        print("RegularMessageHandler, Info response received!")
        if not self.is_from_trusted_contact(msg):
            return
        sender_id = msg.get_field(msg.FIELD_SENDER_ID)
        print("Info response from '%s'" % sender_id)
        if msg.get_field(msg.FIELD_INFOTYPE) == msg.INFO_PROFILE:
            print("  It's a profile info: '%s'" % msg.get_field(msg.FIELD_RESULT))

    def receive_friend_referral(self, msg):
        '''Receive a friend referral'''
        print("Received friend referral")
        if not self.is_from_trusted_contact(msg):
            print("Received friend referral but sender wasn't trusted?!")
            return
        suggested_friendid = msg.get_field(msg.FIELD_FRIEND_ID)
        print("Received referral for '%s'" % suggested_friendid)
        current_status = self._get_contact_status(suggested_friendid)
        print("Current status of this contact is '%s'" % current_status)
        if msg.is_normal_referral():
            print("It's a regular friend referral")
        elif msg.is_robot_referral():
            print("My friend has referred their robot to me!")
        elif msg.is_robot_removal():
            print("My friend wants to remove their robot!")

    def receive_friend_refer_request(self, msg):
        '''Receive a friend referral request'''
        if self.is_from_trusted_contact(msg):
            requested_friendid = msg.get_field(msg.FIELD_FRIEND_ID)
            print("Received refer request for '%s'" % requested_friendid)
            if self._get_contact_status(requested_friendid) == 'trusted':
                dbutils.add_message_to_inbox(msg, self.get_component(System.COMPNAME_DATABASE),
                                             inbox.MC_REFERREQ_INCOMING)

    def receive_regular_message(self, msg):
        '''Receive a regular message'''
        if self.is_from_known_contact(msg):
            # sender could be trusted or untrusted
            dbutils.add_message_to_inbox(msg, self.get_component(System.COMPNAME_DATABASE),
                                         inbox.MC_NORMAL_INCOMING)
