'''Main window for Murmeli GUI
   Copyright activityworkshop.net and released under the GPL v2.'''

import os
import shutil
from PyQt5 import QtWidgets, QtGui, QtCore
from PyQt5.QtWidgets import QApplication
from murmeli.config import Config
from murmeli.contacts import Contacts
from murmeli.contactcheck import ContactChecker
from murmeli.cryptoclient import CryptoClient
from murmeli.gui import GuiWindow
from murmeli.guinotification import GuiNotifier
from murmeli.i18n import I18nManager
from murmeli.logger import Logger
from murmeli.loggergui import GuiLogSink
from murmeli.messagehandler import RegularMessageHandler
from murmeli.pages import PageServer
from murmeli.postservice import PostService
from murmeli.supersimpledb import MurmeliDb
from murmeli.system import System
from murmeli.torclient import TorClient


class MainWindow(GuiWindow):
    '''Class for the main GUI window using Qt'''

    def __init__(self, system, *args):
        '''Constructor'''
        self.log_panel = GuiLogSink()
        GuiWindow.__init__(self, lower_item=self.log_panel)
        self.system = self.ensure_system(system)
        # we want to be notified of Config changes
        self.system.invoke_call(System.COMPNAME_CONFIG,
                                "add_listener", sub=self)
        self.toolbar_actions = []
        title = self.system.invoke_call(System.COMPNAME_I18N, "get_text",
                                        key="mainwindow.title")
        self.setWindowTitle(title or "Cannot get texts")
        self.clear_web_cache()
        self.toolbar = self.make_toolbar([
            ("toolbar-home.png", self.on_home_clicked, "mainwindow.toolbar.home"),
            ("toolbar-people.png", self.on_contacts_clicked, "mainwindow.toolbar.contacts"),
            ("toolbar-messages.png", self.on_messages_clicked, "mainwindow.toolbar.messages"),
            ("toolbar-messages-highlight.png", self.on_messages_clicked,
             "mainwindow.toolbar.messages"),
            ("toolbar-calendar.png", self.on_calendar_clicked, "mainwindow.toolbar.calendar"),
            ("toolbar-settings.png", self.on_settings_clicked, "mainwindow.toolbar.settings")
        ])
        self.addToolBar(self.toolbar)
        self.modify_toolbar(False)
        self.setContextMenuPolicy(QtCore.Qt.NoContextMenu)

        self.show_page("<html><body><h1>Murmeli</h1><p>Welcome to Murmeli.</p></body></html>")

        self.set_page_server(PageServer(self.system))
        self.navigate_to("/")
        self._trigger_robot_check()

    def finish(self):
        '''Close the window, finish off'''
        if self.system:
            self.system.stop()

    def ensure_system(self, system):
        '''Make sure that we have a complete system'''
        my_system = system or System()
        # Add i18n
        if not my_system.has_component(System.COMPNAME_I18N):
            i18n = I18nManager(my_system)
            my_system.add_component(i18n)
        # Add config
        if not my_system.has_component(System.COMPNAME_CONFIG):
            config = Config(my_system)
            my_system.add_component(config)
        # Add database
        if not my_system.has_component(System.COMPNAME_DATABASE):
            db_file_path = my_system.invoke_call(System.COMPNAME_CONFIG, "get_ss_database_file")
            if os.path.exists(db_file_path):
                database = MurmeliDb(system, db_file_path)
                my_system.add_component(database)
        # Add crypto
        if not my_system.has_component(System.COMPNAME_CRYPTO):
            crypto = CryptoClient(my_system)
            my_system.add_component(crypto)
        if not my_system.has_component(System.COMPNAME_MSG_HANDLER):
            msg_handler = RegularMessageHandler(my_system)
            my_system.add_component(msg_handler)
        # Add tor proxy service
        if not my_system.has_component(System.COMPNAME_TOR):
            config = my_system.get_component(System.COMPNAME_CONFIG)
            tor_client = TorClient(my_system, config.get_tor_dir(),
                                   config.get_property(config.KEY_TOR_EXE))
            my_system.add_component(tor_client)
        # Add contact list
        if not my_system.has_component(System.COMPNAME_CONTACTS):
            contacts = Contacts(my_system)
            my_system.add_component(contacts)
        # Add post service
        if not my_system.has_component(System.COMPNAME_POSTSERVICE):
            post = PostService(my_system)
            my_system.add_component(post)
        # Add gui notifier
        my_system.remove_component(System.COMPNAME_GUI)
        notifier = GuiNotifier(my_system, self)
        my_system.add_component(notifier)
        # Add log
        if not my_system.has_component(System.COMPNAME_LOGGING):
            logger = Logger(my_system)
            logger.add_sink(self.log_panel)
            my_system.add_component(logger)
        # Use config to activate current language
        my_system.invoke_call(System.COMPNAME_I18N, "set_language")
        return my_system

    def make_toolbar(self, deflist):
        '''Given a list of (image, method, tooltip), make a QToolBar with those actions'''
        toolbar = QtWidgets.QToolBar(self)
        toolbar.setFloatable(False)
        toolbar.setMovable(False)
        toolbar.setIconSize(QtCore.QSize(48, 48))
        self.toolbar_actions = []
        for action_def in deflist:
            action = toolbar.addAction(QtGui.QIcon("images/" + action_def[0]), "_", action_def[1])
            action.tooltip_key = action_def[2]
            self.toolbar_actions.append(action)
        self.config_updated()  # to set the tooltips
        return toolbar

    def clear_web_cache(self):
        '''Delete all the files in the web cache'''
        cache_dir = self.system.invoke_call(System.COMPNAME_CONFIG,
                                            "get_web_cache_dir")
        # TODO: Check whether this is a valid web cache dir before deleting!
        shutil.rmtree(cache_dir, ignore_errors=True)
        os.makedirs(cache_dir)

    def modify_toolbar(self, highlight_messages):
        '''Either highlight (if flag True) or not-highlight the new messages toolbar icon'''
        self.toolbar.actions()[2].setVisible(not highlight_messages)
        self.toolbar.actions()[3].setVisible(highlight_messages)

    def on_home_clicked(self):
        '''home button on toolbar clicked'''
        self.navigate_to("/")
    def on_contacts_clicked(self):
        '''contacts button on toolbar clicked'''
        self.navigate_to("/contacts/")
    def on_messages_clicked(self):
        '''messages button on toolbar clicked'''
        self.navigate_to("/messages/")
    def on_calendar_clicked(self):
        '''calendar button on toolbar clicked'''
        self.navigate_to("/calendar/")
    def on_settings_clicked(self):
        '''settings button on toolbar clicked'''
        self.navigate_to("/settings/")

    def config_updated(self):
        '''React to changes in config by changing tooltips'''
        self.system.invoke_call(System.COMPNAME_I18N, "set_language")
        for action in self.toolbar_actions:
            tip = self.system.invoke_call(System.COMPNAME_I18N, "get_text", key=action.tooltip_key)
            action.setToolTip(tip)
        if self.system.invoke_call(System.COMPNAME_CONFIG, "get_property",
                                   key=Config.KEY_SHOW_LOG_WINDOW):
            self.log_panel.show()
        else:
            self.log_panel.hide()

    def _trigger_robot_check(self):
        '''Trigger a check of our robot's contacts'''
        database = self.system.get_component(System.COMPNAME_DATABASE)
        crypto = self.system.get_component(System.COMPNAME_CRYPTO)
        ContactChecker(database, crypto).check_robot_connections()


if __name__ == "__main__":
    # Get ready to launch a Qt GUI
    APP = QApplication([])

    WIN = MainWindow(None)
    WIN.show()

    APP.exec_()

    # Window has closed, stop the system
    WIN.finish()
