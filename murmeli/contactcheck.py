'''Module for functions checking contact consistency'''

from murmeli import dbutils
from murmeli.signals import Timer
from murmeli.contactmgr import ContactManager
from murmeli import contactutils


class ContactChecker:
    '''Class to check consistency of contacts, including robot referrals'''
    def __init__(self, database, crypto):
        self.database = database
        self.crypto = crypto

    def check_robot_connections(self):
        '''Trigger a check of our robot's connections'''
        if self.database and self.crypto:
            Timer(60, self._check_robot_connections, repeated=False)

    def _check_robot_connections(self):
        '''Do the actual connection check'''
        print("Checking robot connections...")
        my_robot_id = dbutils.get_own_robot_id(self.database)
        num_sent = 0
        if my_robot_id:
            robot_profile = self.database.get_profile(my_robot_id)
            robots_contacts = contactutils.get_ids(robot_profile.get('contactlist'))
            for prof in dbutils.get_messageable_profiles(self.database):
                if prof.get('status') == 'trusted':
                    # Check if the messages need to be sent, does the robot know the friend?
                    friend_id = prof.get('torid')
                    if friend_id not in robots_contacts:
                        mgr = ContactManager(self.database, self.crypto)
                        mgr.send_robot_referral_messages(my_robot_id, friend_id)
                        num_sent += 1
        print("Robot connections checked, %d message pairs sent" % num_sent)

    def remove_robot_connections(self):
        '''Broadcast messages to tell friends to remove connections to our robot'''
        my_robot_id = dbutils.get_own_robot_id(self.database)
        if not my_robot_id:
            # Nothing to do if we've got no robot
            return
        print("Removing robot connections...")
        for prof in dbutils.get_messageable_profiles(self.database):
            if prof.get('status') == 'trusted':
                friend_id = prof.get('torid')
                mgr = ContactManager(self.database, self.crypto)
                mgr.send_robot_removal_message(my_robot_id, friend_id)
