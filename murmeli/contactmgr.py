'''Module for the management of contacts within Murmeli'''

from murmeli import contactutils
from murmeli import dbutils
from murmeli import inbox
from murmeli import pendingtable
from murmeli.message import Message, ContactAcceptMessage
from murmeli.message import ContactReferralMessage, ContactReferRequestMessage
from murmeli.decrypter import DecrypterShim


class ContactManager:
    '''Class to manage contacts, like processing friend acceptance or rejection,
       working out which contacts are shared, etc'''

    def __init__(self, database, crypto):
        '''Constructor'''
        self._database = database
        self._crypto = crypto

    def handle_initiate(self, tor_id, display_name, robot=False):
        '''We have requested contact with another id, so we can set up
           this new contact's name with the right status'''
        allowed_status = ['robot'] if robot else ['deleted']
        new_status = 'reqrobot' if robot else 'requested'
        self._handle_initiate(tor_id, display_name, allowed_status, new_status)
        if robot:
            own_torid = dbutils.get_own_tor_id(self._database)
            dbutils.update_profile(self._database, own_torid, {'robot':tor_id})

    def _handle_initiate(self, tor_id, display_name, allowed_status, new_status):
        '''We have requested contact with another id, so we can set up
           this new contact's name with a status of "requested"'''
        # If row already exists then get status (and name/displayname) and error with it
        if self._database:
            curr_status = dbutils.get_status(self._database, tor_id)
            if curr_status and curr_status != new_status and curr_status not in allowed_status:
                print("Initiate contact with '%s' but status is already '%s' ?"
                      % (tor_id, curr_status))
                return
        # Add new row in db with id, name and "requested" status
        if tor_id and tor_id != dbutils.get_own_tor_id(self._database):
            display_name = display_name or tor_id
            profile = {'displayName':display_name, 'name':display_name,
                       'status':new_status}
            dbutils.create_profile(self._database, tor_id, profile)


    def handle_accept(self, tor_id):
        '''We want to accept a contact request, so we need to find the request(s),
           and use it/them to update our keyring and our database entry'''
        name, key_str, direct_request = self.get_contact_request_details(tor_id)
        key_valid = key_str and len(key_str) > 20
        print("ContactMgr.handle_accept for name '%s' (direct=%s)" % (name, direct_request))
        # TODO: do we need to handle direct requests differently?
        if key_valid:
            # Get this person's current status from the db, if available
            status = dbutils.get_status(self._database, tor_id)
            print("ContactMgr.handle_accept, status=", status)
            if status in [None, "requested", "deleted"]:
                # Import the found key into the keyring
                key_id = self._crypto.import_public_key(key_str)
                print("Imported key into keyring, got id:", key_id)
                display_name = name or tor_id
                profile = {'displayName':display_name, 'name':display_name,
                           'status':'untrusted', 'keyid':key_id}
                dbutils.create_profile(self._database, tor_id, profile)
            elif status == "pending":
                print("Request already pending, nothing to do")
            else:
                # status could be untrusted, or trusted
                # set status to untrusted?  Send response?
                print("Trying to handle an accept but status is already", status)
            # Move all corresponding requests to be regular messages instead
            dbutils.change_conreq_msgs_to_regular(self._database, tor_id)
        else:
            print("Trying to handle an accept but key isn't valid")
        # Maybe there is a pending contact response to deal with
        self.process_pending_contacts(tor_id)

    def is_robot_id(self, tor_id):
        '''Return True if this tor_id is configured as our robot'''
        if not self._database:
            return False
        own_profile = self._database.get_profile(None)
        if not own_profile or own_profile.get('robot') != tor_id:
            return False
        # Now check other profile
        robot_status = dbutils.get_status(self._database, tor_id)
        return robot_status in ['robot', 'reqrobot']

    def process_pending_contacts(self, tor_id):
        '''Perhaps some contact responses are pending, deal with them now'''
        print("Process pending contact accept responses from:", tor_id)
        found = False
        for resp in self._database.get_pending_contact_messages():
            if resp and resp.get(pendingtable.FN_FROM_ID) == tor_id:
                payload = resp.get(pendingtable.FN_PAYLOAD)
                msg = Message.from_encrypted_payload(payload, DecrypterShim(self._crypto))
                if msg and isinstance(msg, ContactAcceptMessage):
                    found = True
                    # Construct inbox message and pass to db
                    dbutils.add_message_to_inbox(msg, self._database,
                                                 inbox.MC_CONRESP_ALREADY_ACCEPTED)
        if found:
            print("Found pending contact accept from:", tor_id)
            dbutils.update_profile(self._database, tor_id, {'status':'untrusted'})
            # delete_from_pending_table
            self._database.delete_from_pending_table(tor_id)

    def handle_deny(self, tor_id):
        '''We want to deny a contact request - remember that this id is blocked'''
        print("ContactManager: we refused request from '%s'" % tor_id)
        # TODO: Create new profile with status 'blocked' to remember the refusal?

    def delete_contact(self, tor_id):
        '''Set the specified contact's status to deleted'''
        print("ContactManager: set status of '%s' to 'deleted'" % tor_id)
        dbutils.update_profile(self._database, tor_id, {'status':'deleted'})

    def handle_receive_accept(self, tor_id, name, key_str):
        '''We have requested contact with another id, and this has now been accepted.
           So we can import their public key into our keyring and update their status
           accordingly.'''
        key_id = self._crypto.import_public_key(key_str)
        print("Imported key into keyring, got id:", key_id)
        new_status = 'robot' if self.is_robot_id(tor_id) else "untrusted"
        profile = {'status':new_status, 'keyid':key_id, 'name':name}
        dbutils.update_profile(self._database, tor_id, profile)

    def handle_receive_deny(self, tor_id):
        '''We have requested contact with another id, but this has been denied.
           So we need to update their status accordingly'''
        self.delete_contact(tor_id)
        print("ContactMgr received contact refusal from %s" % tor_id)

    def key_fingerprint_checked(self, tor_id):
        '''The fingerprint of this contact's key has been checked (over a separate channel)'''
        # Check that userid exists and that status is ok
        curr_status = dbutils.get_status(self._database, tor_id)
        if curr_status in ["untrusted", "trusted"]:
            dbutils.update_profile(self._database, tor_id, {'status':'trusted'})

    def handle_delete_contact(self, tor_id):
        '''We don't trust this contact any more, so status is set to "deleted"'''
        # TODO: Should status be 'deleted' or 'blocked'?
        self.delete_contact(tor_id)

    def get_contact_request_details(self, tor_id):
        '''Use all received contact requests for the given id, and summarize name and public key'''
        found_names = set()
        found_keys = set()
        direct_request = False
        # Loop through all contact requests and contact refers for the given torid
        for msg in self._database.get_inbox():
            msg_type = msg.get(inbox.FN_MSG_TYPE) if msg else None
            if msg_type == "contactrequest" and msg.get(inbox.FN_FROM_ID) == tor_id:
                found_names.add(msg.get(inbox.FN_FROM_NAME))
                found_keys.add(msg.get(inbox.FN_PUBLIC_KEY))
                direct_request = True
            elif msg_type == "contactrefer" and msg.get(inbox.FN_FRIEND_ID) == tor_id:
                found_names.add(msg.get(inbox.FN_FRIEND_NAME))
                found_keys.add(msg.get(inbox.FN_PUBLIC_KEY))
        if len(found_keys) != 1:
            return (None, None, direct_request)  # no keys or more than one key!
        supplied_key = found_keys.pop()
        if supplied_key is None or len(supplied_key) < 80:
            return (None, None, direct_request)  # one key supplied but it's missing or too short
        supplied_name = found_names.pop() if len(found_names) == 1 else tor_id
        return (supplied_name, supplied_key, direct_request)

    def get_shared_possible_contacts(self, tor_id):
        '''Check which contacts we share with the given torid
           and which ones we could recommend to each other'''
        name_map = {}
        our_contact_ids = set()
        trusted_contact_ids = set()
        their_contact_ids = set()
        # Get our id so we can exclude it from the sets
        my_tor_id = dbutils.get_own_tor_id(self._database)
        if tor_id == my_tor_id:
            return ([], [], [], {})
        # Find the contacts of the specified person
        selected_profile = self._database.get_profile(tor_id)
        selected_contacts = selected_profile.get('contactlist') if selected_profile else None
        for found_id, found_name in contactutils.get_ids_and_names(selected_contacts):
            if found_id != my_tor_id:
                their_contact_ids.add(found_id)
                name_map[found_id] = found_name
        found_their_contacts = True if their_contact_ids else False
        # Now get information about our contacts
        for cont in dbutils.get_messageable_profiles(self._database):
            found_id = cont['torid']
            our_contact_ids.add(found_id)
            if cont.get('status') == 'trusted' and found_id != tor_id:
                trusted_contact_ids.add(found_id)
            name_map[found_id] = cont.get('displayName')
            # Should we check the contact information too?
            if not found_their_contacts:
                if tor_id in contactutils.get_ids(cont.get('contactlist')):
                    their_contact_ids.add(found_id)
        # Now we have three sets of torids: our contacts, our trusted contacts, and their contacts.
        shared_contact_ids = our_contact_ids.intersection(their_contact_ids) # might be empty
        # if the contact isn't trusted, then don't suggest anything
        if not selected_profile or selected_profile.get('status') != 'trusted':
            return (shared_contact_ids, [], [], name_map)
        suggestions_for_them = trusted_contact_ids.difference(their_contact_ids)
        possible_for_me = their_contact_ids.difference(our_contact_ids)
        # These sets may be empty, but we still return the map so we can look up names
        return (shared_contact_ids, suggestions_for_them, possible_for_me, name_map)

    def send_referral_messages(self, friend_id1, friend_id2, intro):
        '''Send messages to both friends, to recommend they become friends with each other'''
        if not friend_id1 or not friend_id2 or friend_id1 == friend_id2:
            return
        if dbutils.get_status(self._database, friend_id1) != 'trusted':
            return
        if dbutils.get_status(self._database, friend_id2) != 'trusted':
            return
        self._send_referral_message(friend_id1, friend_id2, intro)
        self._send_referral_message(friend_id2, friend_id1, intro)

    def _send_referral_message(self, recipient_id, friend_id, intro, refer_type=None):
        '''Send a single referral message'''
        print("Send msg to '%s' referring '%s' with msg '%s' and type '%s'" % \
          (recipient_id, friend_id, intro, refer_type))

    def send_refer_request_message(self, send_to_id, requested_id, intro):
        '''Send a message to send_to_id, to ask that they recommend you to requested_id'''
        if not send_to_id or not requested_id or send_to_id == requested_id:
            return
        if dbutils.get_status(self._database, send_to_id) != 'trusted':
            return
        print("Send msg to '%s' requesting '%s' with msg '%s'" % (send_to_id, requested_id, intro))
        outmsg = ContactReferRequestMessage()
        outmsg.set_field(outmsg.FIELD_MSGBODY, intro)
        outmsg.set_field(outmsg.FIELD_FRIEND_ID, requested_id)
        outmsg.recipients = [send_to_id]
        dbutils.add_message_to_outbox(outmsg, self._crypto, self._database)

    @staticmethod
    def get_contact_name_from_profile(profile, tor_id):
        '''If the given profile has a contact list, use it to look up the name
           for the given tor_id'''
        contact_list = profile.get("contactlist") if profile else None
        if contact_list and isinstance(contact_list, str):
            for contact in contact_list.split(","):
                if contact and len(contact) > 16 and contact[0:16] == tor_id:
                    return contact[16:]
        # The name wasn't found, so just use the tor_id instead
        return tor_id

    def send_robot_referral_messages(self, robot_id, friend_id, add=True):
        '''Send message to friend, to ask them to add or remove our robot'''
        if not robot_id or not friend_id or robot_id == friend_id:
            return
        robot_status = dbutils.get_status(self._database, robot_id)
        friend_status = dbutils.get_status(self._database, friend_id)
        if robot_status != 'robot' or friend_status != 'trusted':
            return
        msg_type = ContactReferralMessage.REFERTYPE_ROBOT if add else \
                   ContactReferralMessage.REFERTYPE_REMOVEROBOT
        self._send_referral_message(friend_id, robot_id, "robot", msg_type)
        if add:
            self._send_referral_message(robot_id, friend_id, "robot", msg_type)


    def send_robot_removal_message(self, robot_id, friend_id):
        '''Send message to friend, to ask them to remove our robot'''
        self.send_robot_referral_messages(robot_id, friend_id, add=False)

    def check_all_contacts_keys(self):
        '''Return a list of names for which the key can't be found'''
        names = []
        for cont in dbutils.get_messageable_profiles(self._database):
            tor_id = cont['torid'] if cont else None
            if tor_id:
                key_id = cont.get('keyid')
                if not key_id or (self._crypto and not self._crypto.get_public_key(key_id)):
                    names.append(cont['displayName'])
                    # We haven't got their key in our keyring!
                    dbutils.update_profile(self._database, tor_id, {"status":"requested"})
        return names
