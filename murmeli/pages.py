'''Module for the pages provided by the system'''

import os
import shutil
import datetime
import re
from PyQt5.QtWidgets import QFileDialog # for file selection
from murmeli.pagetemplate import PageTemplate
from murmeli import dbutils
from murmeli.fingerprints import FingerprintChecker
from murmeli.contactmgr import ContactManager
from murmeli.compose import ComposeWindow
from murmeli.messageutils import MessageTree
from murmeli.brainstorm import StormWindow, FriendStorm
from murmeli import contactutils
from murmeli import cryptoutils
from murmeli import message
from murmeli import inbox


class Bean:
    '''Class for interacting with page templates by adding properties'''
    pass


class PageServer:
    '''PageServer, containing several page sets'''
    def __init__(self, system):
        self.page_sets = {}
        self.add_page_set(DefaultPageSet(system))
        self.add_page_set(ContactsPageSet(system))
        self.add_page_set(MessagesPageSet(system))
        self.add_page_set(SettingsPageSet(system))
        self.add_page_set(ComposePageSet(system))
        self.add_page_set(SpecialFunctions(system))
        self.add_page_set(TestPageSet(system))
        # keep track of the windows we have opened (so they're not garbage-collected)
        self.extra_windows = set()

    def add_page_set(self, pageset):
        '''Add a page set'''
        self.page_sets[pageset.domain] = pageset

    def serve_page(self, view, url, params):
        '''Serve the page associated with the given url and parameters'''
        domain, path = self.get_domain_and_path(url)
        # Do I need to intercept this to create a new window?
        if domain == "new":
            window_title = self.get_page_title(path)
            compwin = ComposeWindow(window_title or "Murmeli")
            compwin.set_page_server(self)
            compwin.show_page("<html></html>")
            compwin.navigate_to(path, params)
            self.extra_windows.add(compwin)
            # Remove invisible (closed) windows
            self.extra_windows = set([win for win in self.extra_windows if win.isVisible()])
            return
        page_set = self.page_sets.get(domain)
        if not page_set:
            page_set = self.page_sets.get("")
        page_set.serve_page(view, path, params)

    @staticmethod
    def get_domain_and_path(url):
        '''Extract the domain and path from the given url'''
        stripped_url = url.strip() if url else ""
        if stripped_url.startswith("http://murmeli/"):
            stripped_url = stripped_url[15:]
        while stripped_url.startswith("/"):
            stripped_url = stripped_url[1:]
        slashpos = stripped_url.find("/")
        if slashpos < 0:
            return (stripped_url, '')
        return (stripped_url[:slashpos], stripped_url[slashpos + 1:])

    def get_page_title(self, path):
        '''Get the title of the specified page from one of the pagesets'''
        domain, subpath = self.get_domain_and_path(path)
        page_set = self.page_sets.get(domain)
        return page_set.get_page_title(subpath) if page_set else None


class PageSet:
    '''Superclass of all page sets'''

    def __init__(self, system, domain):
        self.system = system
        self.domain = domain
        self.std_head = ("<html><head>"
                         "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">"
                         "<link href='file:///" + self.get_web_cache_dir() + "/default.css'"
                         " type='text/css' rel='stylesheet'>"
                         "<script type='text/javascript'>"
                         "function hideOverlay(){"
                         " showLayer('overlay',false);showLayer('popup',false)}"
                         " function showLayer(lname,show){"
                         " document.getElementById(lname).style.visibility="
                         "(show?'visible':'hidden');}"
                         " function showMessage(mess){"
                         " document.getElementById('popup').innerHTML=mess;"
                         " showLayer('overlay',true); showLayer('popup', true);}"
                         "</script></head>")

    def require_resource(self, resource):
        '''Require that the specified resource should be copied from web to the cache directory'''
        cache_dir = self.get_web_cache_dir()
        if cache_dir:
            dest_path = os.path.join(cache_dir, resource)
            if not os.path.exists(dest_path):
                # dest doesn't exist
                # (if it exists we assume it's still valid as these resources shouldn't change)
                source_path = os.path.join("web", resource)
                # TODO: This fails if dest directory doesn't exist - eg if Config has become blank?
                if os.path.exists(source_path):
                    shutil.copy(source_path, dest_path)
                else:
                    print("OUCH - failed to copy resource '%s' from web!" % resource)

    def get_web_cache_dir(self):
        '''Get the web cache directory from the config'''
        cache = None
        if self.system:
            cache = self.system.invoke_call(self.system.COMPNAME_CONFIG, "get_web_cache_dir")
        return cache or ""

    def require_resources(self, resources):
        '''Require a list of page resources such as images, css'''
        if isinstance(resources, list):
            for res in resources:
                self.require_resource(res)
        elif isinstance(resources, str):
            self.require_resource(resources)

    def build_page(self, params):
        '''General page-building method using a standard template
           and filling in the gaps using the given dictionary'''
        self.require_resource("default.css")
        return ''.join([self.std_head,
                        "<body>",
                        "<table border='0' width='100%%'>"
                        "<tr><td><div class='fancyheader'><p>%(pageTitle)s</p></div></td></tr>",
                        "<tr><td><div class='genericbox'>%(pageBody)s</div></td></tr>",
                        "<tr><td><div class='footer'>%(pageFooter)s</div></td></tr></table>",
                        "<div class='overlay' id='overlay' onclick='hideOverlay()'></div>",
                        "<div class='popuppanel' id='popup'>Here's the message</div>",
                        "</body></html>"]) % params

    def build_two_column_page(self, params):
        '''Build page using a two-column template with widths 1third, 2thirds'''
        self.require_resource("default.css")
        return ''.join([self.std_head,
                        "<body>",
                        "<table border='0' width='100%%'>"
                        "<tr><td colspan='2'>"
                        "<div class='fancyheader'><p>%(pageTitle)s</p></div></td></tr>",
                        "<tr valign='top'><td width='33%%'>"
                        "<div class='genericbox'>%(leftColumn)s</div></td>",
                        "<td width='67%%'><div class='genericbox'>%(rightColumn)s</div></td></tr>",
                        "<tr><td colspan='2'>"
                        "<div class='footer'>%(pageFooter)s</div></td></tr>"
                        "</table>",
                        "<div class='overlay' id='overlay' onclick='hideOverlay()'></div>",
                        "<div class='popuppanel' id='popup'>Here's the message</div>",
                        "</body></html>"]) % params

    def make_local_time_string(self, tstamp):
        '''Convert a float (in UTC) to a string (in local timezone) for display'''
        if not tstamp:
            return ""
        try:
            send_time = datetime.datetime.fromtimestamp(tstamp)
            # Check if it's today
            now = datetime.datetime.now()
            midnight = datetime.datetime(now.year, now.month, now.day, 0, 0, 0)
            if send_time.timestamp() > midnight.timestamp():
                # today, just show time
                return "%02d:%02d" % (send_time.hour, send_time.minute)
            # Check if it's yesterday
            midnight -= datetime.timedelta(days=1)
            if send_time.timestamp() > midnight.timestamp():
                # yesterday, show 'Yesterday' and time
                return self.i18n("messages.sendtime.yesterday") + \
                       " %02d:%02d" % (send_time.hour, send_time.minute)
            # Not today or yesterday, show full date and time
            return "%d-%02d-%02d %02d:%02d" % (send_time.year, send_time.month, send_time.day,
                                               send_time.hour, send_time.minute)
        except TypeError:
            print("Expected a float timestamp, found", type(tstamp), repr(tstamp))
        if isinstance(tstamp, str):
            return tstamp
        return ""

    def i18n(self, key):
        '''Use the i18n component to translate the given key'''
        if self.system:
            return self.system.invoke_call(self.system.COMPNAME_I18N, "get_text", key=key)
        return None

    def get_all_i18n(self):
        '''Use the i18n component to get all the texts'''
        if self.system:
            return self.system.invoke_call(self.system.COMPNAME_I18N, "get_all_texts")
        return {}

    def get_config(self):
        '''Get the config component, if any'''
        if self.system:
            return self.system.get_component(self.system.COMPNAME_CONFIG)
        return None

    @staticmethod
    def looks_like_userid(userid):
        '''Return true if the given string looks like a valid user id'''
        if userid and len(userid) == 16 and re.match("([a-zA-Z0-9]+)$", userid):
            return True
        return False

    @staticmethod
    def get_param_as_int(params, param_name, default_value=-1):
        '''Get the value of the specified parameter as an int'''
        if params and isinstance(params, dict) and param_name:
            param_val = params.get(param_name)
            if param_val and isinstance(param_val, str):
                try:
                    return int(param_val)
                except ValueError:
                    pass
        return default_value

    def get_page_title(self, path):
        '''Get the page title for the given path'''
        return None


class DefaultPageSet(PageSet):
    '''Default page server, just for home page'''
    def __init__(self, system):
        PageSet.__init__(self, system, "")
        self.hometemplate = PageTemplate('home')

    def serve_page(self, view, url, params):
        '''Serve a page to the given view'''
        self.require_resource('avatar-none.jpg')
        _ = (url, params)
        page_title = self.i18n("home.title") or ""
        tokens = self.get_all_i18n()
        contents = self.build_page({'pageTitle':page_title,
                                    'pageBody':self.hometemplate.get_html(tokens),
                                    'pageFooter':"<p>Footer</p>"})
        view.set_html(contents)
        self._check_contacts(view)

    def _check_contacts(self, view):
        '''Check all our contacts and see if any keys are missing'''
        database = self.system.get_component(self.system.COMPNAME_DATABASE)
        crypto = self.system.get_component(self.system.COMPNAME_CRYPTO)
        missing_names = ContactManager(database, crypto).check_all_contacts_keys()
        if missing_names:
            texts = self.i18n("warning.keysnotfoundfor") + " " + ", ".join(missing_names)
            view.page().runJavaScript("window.alert('%s');" % texts.replace("'", "\\'"))


class ContactsPageSet(PageSet):
    '''Contacts page server, for showing list of contacts etc'''
    def __init__(self, system):
        PageSet.__init__(self, system, "contacts")
        self.listtemplate = PageTemplate('contactlist')
        self.detailstemplate = PageTemplate('contactdetails')
        self.editowndetailstemplate = PageTemplate('editcontactself')
        self.editdetailstemplate = PageTemplate('editcontact')
        self.addtemplate = PageTemplate('addcontact')
        self.addrobottemplate = PageTemplate('addrobot')
        self.fingerprintstemplate = PageTemplate('fingerprints')

    def serve_page(self, view, url, params):
        '''Serve a page to the given view'''
        print("Contacts serving page", url)
        self.require_resources(['button-addperson.png', 'button-addrobot.png',
                                'button-drawgraph.png', 'avatar-none.jpg',
                                'jquery-3.3.1.slim.js'])
        database = self.system.get_component(self.system.COMPNAME_DATABASE)
        crypto = self.system.get_component(self.system.COMPNAME_CRYPTO)
        dbutils.export_all_avatars(database, self.get_web_cache_dir())
        contents = None
        page_params = {}
        commands = self.interpret_commands(url)
        userid = commands[1] if len(commands) == 2 else None
        if commands[0] == "add":
            contents = self.make_add_page()
        elif commands[0] == "submitadd":
            recipientid = None
            if params:
                print("params for submit add:", params)
                # request to add a new friend
                recipientid = params.get('murmeliid', '')
                dispname = params.get('displayname', '')
                intromessage = params.get('intromessage', '')
                if self.looks_like_userid(recipientid):
                    # TODO: Check status to see whether to send message or not
                    # TODO: Use ContactManager to update the database with the new contact
                    print("I should send an add request to '%s' now." % recipientid)
                    print("Message is '%s'." % intromessage)
                    print("and the name for the profile is '%s'." % dispname)
                    # TODO: Create request message and add to outbox
                else:
                    print("Hmm, show an error message here?")
                # in any case, go back to contact list

                # ensure that avatar is exported for this new contact
                dbutils.export_all_avatars(database, self.get_web_cache_dir())
            else:
                print("Add request without params?")

            contents = "generate submitadd page for " + recipientid

        elif commands[0] == "exportkey":
            print("Export the key now!")
            own_keyid = dbutils.get_own_key_id(database)
            data_dir = self.get_config().get_data_dir()
            if cryptoutils.export_public_key(own_keyid, data_dir, crypto):
                print("Exported public key")
            else:
                print("FAILED to export public key")
            # Show javascript alert to confirm that export was done
            return
        elif commands[0] == "addrobot":
            contents = self.make_add_robot_page()
        elif commands[0] == "submitaddrobot":
            if params:
                robot_id = params.get('murmeliid')
                print("Requested robot_id = '%s'" % robot_id)
        elif commands[0] == "edit":
            contents = self.make_list_page(do_edit=True, userid=userid)
        elif commands[0] == "submitedit":
            dbutils.update_profile(self.system.get_component(self.system.COMPNAME_DATABASE),
                                   tor_id=commands[1], in_profile=params,
                                   pic_output_path=self.get_web_cache_dir())

        elif commands[0] == "checkfingerprint":
            contents = self.make_checkfinger_page(commands[1])
        elif commands[0] == "checkedfingerprint":
            given_answer = self.get_param_as_int(params, "answer", -1)
            fingers = self._make_fingerprint_checker(userid)
            expected_answer = fingers.get_correct_answer()
            print("checked fingerprint with answer '%d', correct answer is '%d'" \
                  % (given_answer, expected_answer))
            # Compare with expected answer, generate appropriate page
            if given_answer == expected_answer:
                ContactManager(database, None).key_fingerprint_checked(userid)
                # Show page again
                contents = self.make_checkfinger_page(userid)
            else:
                print("Wrong fingerprint given, set flag and go back to contacts")
                page_params['fingerprint_check_failed'] = True
        elif commands[0] == "delete" and userid:
            print("Deleting user '%s' now" % userid)
            dbutils.update_profile(self.system.get_component(self.system.COMPNAME_DATABASE),
                                   tor_id=userid, in_profile={'status':'deleted'})
            userid = None
        elif commands[0] == "refer":
            intro = str(params.get('introMessage', ""))
            ContactManager(database, crypto).send_referral_messages(commands[1], commands[2],
                                                                    intro)
        elif commands[0] == "requestrefer":
            intro = str(params.get('introMessage', ""))
            ContactManager(database, crypto).send_refer_request_message(commands[1], commands[2],
                                                                        intro)

        # If we haven't got any contents yet, then do a show details
        if not contents:
            # Show details for selected userid (or for self if userid is None)
            print("Commands:", commands)
            if commands[0] == 'show' and len(commands) == 2:
                userid = commands[1]
            contents = self.make_list_page(do_edit=False, userid=userid, extra_params=page_params)

        view.set_html(contents)

    def interpret_commands(self, url):
        '''Take the url to make a list of command to execute and its parameters'''
        if url:
            command = [elem for elem in url.split("/") if elem]
            if command:
                if len(command) == 1:
                    if command[0] in ['add', 'submitadd', 'addrobot', 'submitaddrobot',
                                      'exportkey']:
                        return command
                    if self.looks_like_userid(command[0]):
                        return ['show', command[0]]
                elif len(command) == 2:
                    if self.looks_like_userid(command[0]):
                        if command[1] in ['edit', 'submitedit', 'delete', 'checkfingerprint',
                                          'checkedfingerprint']:
                            return [command[1], command[0]]
                elif len(command) == 3:
                    if self.looks_like_userid(command[0]) and self.looks_like_userid(command[2]):
                        if command[1] in ['refer', 'requestrefer']:
                            return [command[1], command[0], command[2]]
        return ['show', None]

    def make_list_page(self, do_edit=False, userid=None, extra_params=None):
        '''Generate a page for listing all the contacts and showing the details of one of them'''
        self.require_resources(['status-self.png', 'status-requested.png', 'status-untrusted.png',
                                'status-trusted.png', 'status-pending.png', 'status-robot.png'])
        config = self.get_config()

        # Who are we showing?
        selectedprofile = self.system.invoke_call(self.system.COMPNAME_DATABASE, "get_profile",
                                                  torid=userid)
        ownprofile = self.system.invoke_call(self.system.COMPNAME_DATABASE, "get_profile",
                                             torid=None)
        if not selectedprofile:
            selectedprofile = ownprofile
        userid = selectedprofile['torid']
        own_page = userid == ownprofile['torid']

        # Build list of contacts
        userboxes = []
        database = self.system.get_component(self.system.COMPNAME_DATABASE)
        for profile in database.get_profiles():
            if profile['status'] in ['requested', 'untrusted', 'trusted', 'self']:
                box = Bean()
                box.disp_name = profile['displayName']
                tor_id = profile['torid']
                box.torid = tor_id
                box.tilestyle = "contacttile" + ("selected" if profile['torid'] == userid else "")
                box.status = profile['status']
                is_online = self.system.invoke_call(self.system.COMPNAME_CONTACTS,
                                                    "is_online", tor_id=tor_id)
                last_time = self.system.invoke_call(self.system.COMPNAME_CONTACTS,
                                                    "last_seen", tor_id=tor_id)
                box.last_seen = self._make_lastseen_string(is_online, last_time)
                # Find out from database whether this user has a robot or not
                box.has_robot = dbutils.has_robot(database, tor_id)
                userboxes.append(box)
        # build list of contacts on left of page using these boxes
        tokens = self.get_all_i18n()
        lefttext = self.listtemplate.get_html(tokens, {'webcachedir':config.get_web_cache_dir(),
                                                       'contacts':userboxes})

        page_props = {"webcachedir":config.get_web_cache_dir(), 'person':selectedprofile}
        # Add extra parameters if necessary
        if extra_params:
            page_props.update(extra_params)
        # See which contacts we have in common with this person
        results = ContactManager(database, None).get_shared_possible_contacts(userid)
        shared_ids, ids_for_them, ids_for_me, name_map = results
        page_props["sharedcontacts"] = self._make_id_name_bean_list(shared_ids, name_map)
        page_props["posscontactsforthem"] = self._make_id_name_bean_list(ids_for_them, name_map)
        page_props["posscontactsforme"] = self._make_id_name_bean_list(ids_for_me, name_map)

        # Work out status of this contact's robot
        contacts = self.system.get_component(self.system.COMPNAME_CONTACTS)
        robot_status = dbutils.get_robot_status(database, userid, contacts)
        page_props['robotstatus'] = self.i18n("contacts.details.robotid." + robot_status)
        page_props['robotset'] = (robot_status != 'none')

        # Which template to use depends on whether we're just showing or also editing
        if do_edit:
            # Use two different details templates, one for self and one for others
            detailstemplate = self.editowndetailstemplate if own_page else self.editdetailstemplate
            righttext = detailstemplate.get_html(tokens, page_props)
        else:
            righttext = self.detailstemplate.get_html(tokens, page_props)

        # Put left side and right side together
        contents = self.build_two_column_page({'pageTitle':self.i18n("contacts.title"),
                                               'leftColumn':lefttext,
                                               'rightColumn':righttext,
                                               'pageFooter':"<p>Footer</p>"})
        return contents

    @staticmethod
    def _make_id_name_bean_list(cids, name_map):
        '''Make a list of Bean objects for the given contact list'''
        con_list = []
        for cid in cids:
            pair = Bean()
            pair.torid = cid
            pair.disp_name = name_map.get(cid, cid)
            con_list.append(pair)
        return con_list

    def _make_lastseen_string(self, online, last_time):
        '''Make a string describing the online / offline status'''
        curr_time = datetime.datetime.now()
        if last_time and (curr_time-last_time).total_seconds() < 18000:
            token = "contacts.onlinesince" if online else "contacts.offlinesince"
            return self.i18n(token) % str(last_time.timetz())[:5]
        if online:
            return self.i18n("contacts.online")
        return None

    def make_add_page(self):
        '''Build the form page for adding a new contact, using the template'''
        own_profile = self.system.invoke_call(self.system.COMPNAME_DATABASE, "get_profile")
        own_tor_id = own_profile.get("torid") if own_profile else None
        tokens = self.get_all_i18n()
        bodytext = self.addtemplate.get_html(tokens, {"owntorid":own_tor_id or ""})
        return self.build_page({'pageTitle':self.i18n("contacts.title"),
                                'pageBody':bodytext,
                                'pageFooter':"<p>Footer</p>"})

    def make_add_robot_page(self):
        '''Build the form page for adding a new robot, using the template'''
        own_profile = self.system.invoke_call(self.system.COMPNAME_DATABASE, "get_profile") or {}
        own_tor_id = own_profile.get("torid")
        robot_id = own_profile.get("robotid")
        tokens = self.get_all_i18n()
        bodytext = self.addrobottemplate.get_html(tokens, {"owntorid":own_tor_id or "",
                                                           "robotid":robot_id or ""})
        return self.build_page({'pageTitle':self.i18n("contacts.title"),
                                'pageBody':bodytext,
                                'pageFooter':"<p>Footer</p>"})

    def make_checkfinger_page(self, userid):
        '''Generate a page for checking the fingerprint of the given user'''
        # First, get the name of the user
        person = self.system.invoke_call(self.system.COMPNAME_DATABASE, "get_profile",
                                         torid=userid)
        disp_name = person['displayName']
        full_name = person['name']
        if disp_name != full_name:
            full_name = "%s (%s)" % (disp_name, full_name)
        # check it's ok to generate
        status = person.get('status')
        if status not in ['untrusted', 'trusted']:
            print("Not generating fingerprints page because status is", status)
            return None
        fingers = self._make_fingerprint_checker(userid)
        already_checked = (person.get('status') == "trusted")
        # TODO: Provide way to choose another language, not just "en"
        page_params = {"mywords":fingers.get_code_words(True, 0, "en"),
                       "theirwords0":fingers.get_code_words(False, 0, "en"),
                       "theirwords1":fingers.get_code_words(False, 1, "en"),
                       "theirwords2":fingers.get_code_words(False, 2, "en"),
                       "fullname":full_name, "shortname":disp_name, "userid":userid,
                       "alreadychecked":already_checked}
        body_text = self.fingerprintstemplate.get_html(self.get_all_i18n(), page_params)
        return self.build_page({'pageTitle':self.i18n("contacts.title"),
                                'pageBody':body_text,
                                'pageFooter':"<p>Footer</p>"})

    def _make_fingerprint_checker(self, userid):
        '''Use the given userid to make a FingerprintChecker between me and them'''
        own_profile = self.system.invoke_call(self.system.COMPNAME_DATABASE, "get_profile",
                                              torid=None)
        own_fingerprint = self.system.invoke_call(self.system.COMPNAME_CRYPTO, "get_fingerprint",
                                                  key_id=own_profile['keyid'])
        person = self.system.invoke_call(self.system.COMPNAME_DATABASE, "get_profile",
                                         torid=userid)
        other_fingerprint = self.system.invoke_call(self.system.COMPNAME_CRYPTO, "get_fingerprint",
                                                    key_id=person['keyid'])
        return FingerprintChecker(own_fingerprint, other_fingerprint)


class MessagesPageSet(PageSet):
    '''Messages page set, for showing list of messages etc'''
    def __init__(self, system):
        PageSet.__init__(self, system, "messages")
        self.messages_template = PageTemplate('messages')

    def serve_page(self, view, url, params):
        '''Serve a page to the given view'''
        print("Messages serving page", url, "params:", params)
        self.require_resources(['button-compose.png', 'default.css', 'jquery-3.3.1.slim.js',
                                'avatar-none.jpg'])
        database = self.system.get_component(self.system.COMPNAME_DATABASE)
        dbutils.export_all_avatars(database, self.get_web_cache_dir())

        message_list = None
        search_term = None
        if url == 'send' and params['messageType'] == "contactresponse":
            tor_id = params['sendTo']
            print("Send contact response to:", tor_id)
        elif url == 'delete':
            msg_index = self.get_param_as_int(params, 'msgId')
            deleted = database.delete_from_inbox(msg_index)
            print("Delete of inbox message", msg_index, ("succeeded" if deleted else "failed"))
        elif url == "search":
            message_list = dbutils.search_inbox(params.get('searchTerm'), database)
            # Tell output page what we searched for too
            search_term = params.get('searchTerm')

        # Make dictionary to convert ids to names
        contact_names = {cont['torid']:cont['displayName'] for cont in database.get_profiles()}
        unknown_sender = self.i18n("messages.sender.unknown")
        unknown_recpt = self.i18n("messages.recpt.unknown")

        if message_list is None:
            message_list = database.get_inbox() if database else []
        # Get contact requests, responses and mails from inbox
        conreqs = []
        conresps = []
        mail_tree = MessageTree()
        for msg in message_list:
            if not msg or msg.get(inbox.FN_DELETED):
                continue
            timestamp = msg.get(inbox.FN_TIMESTAMP)
            msg[inbox.FN_SENT_TIME_STR] = self.make_local_time_string(timestamp)
            msg_type = msg.get(inbox.FN_MSG_TYPE)
            # Lookup sender name for display
            sender_id = msg.get(inbox.FN_FROM_ID)
            if not msg.get(inbox.FN_FROM_NAME):
                msg[inbox.FN_FROM_NAME] = contact_names.get(sender_id, unknown_sender)
            if msg_type in ["contactrequest", "contactrefer", "referrequest"]:
                if msg_type == "referrequest":
                    msg[inbox.FN_FRIEND_NAME] = contact_names.get(msg.get(inbox.FN_FRIEND_ID))
                conreqs.append(msg)
            elif msg_type == "contactresponse":
                msg[inbox.FN_MSG_BODY] = self.fix_conresp_body(msg.get(inbox.FN_MSG_BODY),
                                                               msg.get(inbox.FN_ACCEPTED))
                conresps.append(msg)
            else:
                print("Found inbox message of type '%s' from sender id '%s' and body '%s'" \
                      % (msg_type, sender_id, msg.get(inbox.FN_MSG_BODY)))
                # Prepare msg with additional fields for display
                recpts = msg.get(inbox.FN_RECIPIENTS)
                if recpts:
                    reply_all = recpts.split(",")
                    recpt_name_list = [contact_names.get(i, unknown_recpt) for i in reply_all]
                    msg[inbox.FN_RECIPIENT_NAMES] = ", ".join(recpt_name_list)
                    reply_all.append(sender_id)
                    msg[inbox.FN_REPLY_ALL] = ",".join(reply_all)
                mail_tree.add_msg(msg)

        mails = mail_tree.build()
        num_msgs = len(conreqs) + len(conresps) + len(mails)
        bodytext = self.messages_template.get_html(self.get_all_i18n(),
                                                   {"contactrequests":conreqs,
                                                    "contactresponses":conresps,
                                                    "mails":mails, "nummessages":num_msgs,
                                                    "searchterm":search_term,
                                                    "webcachedir":self.get_web_cache_dir()})
        contents = self.build_page({'pageTitle':self.i18n("messages.title"),
                                    'pageBody':bodytext,
                                    'pageFooter':"<p>Footer</p>"})
        view.set_html(contents)

    def fix_conresp_body(self, msg_body, accepted):
        '''If a contact response message has a blank message body, replace it'''
        if msg_body:
            return msg_body
        suffix = "acceptednomessage" if accepted else "refused"
        return self.i18n("messages.contactrequest." + suffix)


class SettingsPageSet(PageSet):
    '''Settings page server, for showing the current settings'''
    def __init__(self, system):
        PageSet.__init__(self, system, "settings")
        self.formtemplate = PageTemplate('settingsform')

    def serve_page(self, view, url, params):
        '''Serve a page to the given view'''
        config = self.get_config()
        if not config:
            view.set_html("Settings didn't find the config!")
            return
        if url == "edit":
            selected_lang = params.get('lang')
            if selected_lang and len(selected_lang) == 2:
                config.set_property(config.KEY_LANGUAGE, selected_lang)
                # I18nManager will be triggered here because it listens to the Config
                friendsseefriends = True if params.get('friendsseefriends') else False
                config.set_property(config.KEY_LET_FRIENDS_SEE_FRIENDS, friendsseefriends)
                showlogwindow = True if params.get('showlogwindow') else False
                config.set_property(config.KEY_SHOW_LOG_WINDOW, showlogwindow)
                # If Config has changed, may need to update profile to include/hide friends info
                # DbI.updateContactList(friendsseefriends)
                # When friends are notified next time, the profile's hash will be calculated + sent
                allowfriendrequests = True if params.get('allowfriendrequests') else False
                config.set_property(config.KEY_ALLOW_FRIEND_REQUESTS, allowfriendrequests)
                # Save config to file in case it's changed
                config.save()
                contents = self.build_page({'pageTitle':self.i18n("settings.title"),
                                            'pageBody':"<p>Settings changed... should I go back"
                                                       + " to settings or back to home now?</p>"
                                                       + "<p>(<a href='/'>back</a>)</p>",
                                            'pageFooter':"<p>Footer</p>"})
            view.set_html(contents)
        else:
            page_props = {"friendsseefriends" : \
                            self.check_from_config(config, config.KEY_LET_FRIENDS_SEE_FRIENDS),
                          "allowfriendrequests" : \
                            self.check_from_config(config, config.KEY_ALLOW_FRIEND_REQUESTS),
                          "showlogwindow" : \
                            self.check_from_config(config, config.KEY_SHOW_LOG_WINDOW),
                          "language_en":"",
                          "language_de":""}
            page_props["language_" + config.get_property(config.KEY_LANGUAGE)] = "selected"
            tokens = self.get_all_i18n()
            contents = self.build_page({'pageTitle':self.i18n("settings.title"),
                                        'pageBody':self.formtemplate.get_html(tokens, page_props),
                                        'pageFooter':"<p>Footer</p>"})
            view.set_html(contents)

    @staticmethod
    def check_from_config(config, key):
        '''Get a string either "checked" or "" depending on the config flag'''
        return "checked" if config.get_property(key) else ""


class ComposePageSet(PageSet):
    '''Functions for composing a new message'''
    def __init__(self, system):
        PageSet.__init__(self, system, "compose")
        self.composetemplate = PageTemplate('composemessage')
        self.closingtemplate = PageTemplate('windowclosing')

    def get_page_title(self, path):
        '''Get the page title for the given path'''
        if path == "start":
            return self.i18n("messages.createnew")
        return None

    def serve_page(self, view, url, params):
        '''Serve a page to the given view'''
        if url == "start":
            self.require_resources(['default.css', 'jquery-3.3.1.slim.js'])
            database = self.system.get_component(self.system.COMPNAME_DATABASE)
            dbutils.export_all_avatars(database, self.get_web_cache_dir())
            parent_hash = params.get("reply")
            # Build list of contacts to whom we can send
            userboxes = []
            for profile in self.system.invoke_call(self.system.COMPNAME_DATABASE, "get_profiles"):
                if profile['status'] in ['untrusted', 'trusted']:
                    box = Bean()
                    box.disp_name = profile['displayName']
                    box.torid = profile['torid']
                    userboxes.append(box)
            page_params = {"contactlist":userboxes, "parenthash":parent_hash or "",
                           "webcachedir":self.get_web_cache_dir(),
                           "recipientids":params.get("sendto")}
            tokens = self.get_all_i18n()
            conts = self.build_page({'pageTitle':self.i18n("composemessage.title"),
                                     'pageBody':self.composetemplate.get_html(tokens, page_params),
                                     'pageFooter':"<p>Footer</p>"})
            view.set_html(conts)
            # If we've got no friends, then warn, can't send to anyone
            if not dbutils.has_friends(database):
                view.page().runJavaScript("window.alert('No friends :(');")
        elif url == "send":
            print("Compose pageset, called 'send'")
            msg_body = params.get('messagebody')
            if not msg_body:
                # TODO: throw an exception or just ignore?
                pass
            parent_hash = params.get("parenthash")
            recpts = params.get('sendto')
            # Make a corresponding message object and pass it on
            msg = message.RegularMessage()
            msg.set_field(msg.FIELD_RECIPIENTS, recpts)
            msg.set_field(msg.FIELD_MSGBODY, msg_body)
            msg.set_field(msg.FIELD_REPLYHASH, parent_hash)
            msg.recipients = recpts.split(",")
            print("Send this message:", type(msg))
            crypto = self.system.get_component(self.system.COMPNAME_CRYPTO)
            database = self.system.get_component(self.system.COMPNAME_DATABASE)
            dbutils.add_message_to_outbox(msg, crypto, database)
            # Save a copy of the sent message
            msg.set_field(msg.FIELD_SENDER_ID, dbutils.get_own_tor_id(database))
            dbutils.add_message_to_inbox(msg, database, inbox.MC_NORMAL_SENT)
            # Close window after successful send
            tokens = self.get_all_i18n()
            contents = self.build_page({'pageTitle':self.i18n("messages.title"),
                                        'pageBody':self.closingtemplate.get_html(tokens),
                                        'pageFooter':"<p>Footer</p>"})
            view.set_html(contents)


class SpecialFunctions(PageSet):
    '''Not delivering pages, but calling special Qt functions such as select file
       or launching the wobbly network graph'''
    def __init__(self, system):
        PageSet.__init__(self, system, "special")
        self.storm_win = None

    def serve_page(self, view, url, params):
        '''Serve a page to the given view'''
        if url == "selectprofilepic":
            # Get home directory for file dialog
            homedir = os.path.expanduser("~/")
            fname, _ = QFileDialog.getOpenFileName(view, self.i18n("gui.dialogtitle.openimage"),
                                                   homedir,
                                                   self.i18n("gui.fileselection.filetypes.jpg"))
            if fname:
                # If selected filename has apostrophes in it, these need to be escaped
                if "'" in fname:
                    fname = fname.replace("'", "\\'")
                view.page().runJavaScript("updateProfilePic('%s');" % fname)
        elif url == "friendstorm":
            print("Generate friendstorm")
            database = self.system.get_component(self.system.COMPNAME_DATABASE)
            has_friends = dbutils.has_friends(database)
            print("Friends exist" if has_friends else "No friends exist, can't draw storm")
            self.storm_win = self.create_storm_window(database)
            if self.storm_win:
                self.storm_win.show()
        elif view:
            print("Special function:", url, "params:", params)

    @staticmethod
    def create_storm_window(database):
        '''Create a FriendStorm window using database contents'''
        if not database:
            return None
        own_profile = database.get_profile()
        storm = FriendStorm(own_profile['torid'], own_profile['displayName'])
        # populate storm using database
        for prof in dbutils.get_messageable_profiles(database):
            friend_id = prof.get('torid')
            storm.add_friend(friend_id, prof.get('displayName'))
            friends_friends = prof.get('contactlist')
            if friends_friends:
                for ff_id, ff_name in contactutils.get_ids_and_names(friends_friends):
                    storm.add_friends_friend(friend_id, ff_id, ff_name)
        # Create window and pass storm to it
        win = StormWindow()
        win.set_storm(storm)
        return win


class TestPageSet(PageSet):
    '''Example page server, used for testing link interception and providing diagnostics'''
    def __init__(self, system):
        PageSet.__init__(self, system, "diagnostics")
        self.outbox_template = PageTemplate('showoutbox')
        self.contacts_template = PageTemplate('showcontacts')

    def serve_page(self, view, url, params):
        '''Serve a page to the given view'''
        print("URL= '%s', params='%s'" % (url, repr(params)))
        if url in ['showoutbox', 'deleteoutbox']:
            database = self.system.get_component(self.system.COMPNAME_DATABASE)
            if url == 'deleteoutbox':
                database.delete_all_from_outbox()
            mails = [msg for msg in database.get_outbox() if msg]
            bodytext = self.outbox_template.get_html(self.get_all_i18n(),
                                                     {"mails":mails})
            view.set_html(bodytext)
            return
        if url == 'showcontacts':
            database = self.system.get_component(self.system.COMPNAME_DATABASE)
            profiles = [prof for prof in database.get_profiles()]
            bodytext = self.contacts_template.get_html(self.get_all_i18n(),
                                                       {"profiles":profiles})
            view.set_html(bodytext)
            return
        if url == "output":
            contents = "output page: params='%s'" % repr(params) + \
                       "<p>(<a href='/diagnostics/input'>back to input</a>)</p>"
        else:
            contents = "<h2>Input</h2>" \
                       "<p>Form using 'get' method:</p>" \
                       "<form method='get' action='/diagnostics/output'>" \
                       "<table border='1'>" \
                       "<tr><td>Fruit</td><td><input type='text' name='fruit'></td></tr>" \
                       "<tr><td>Vegetable</td><td><input type='text' name='vegetable'></td></tr>" \
                       "<tr><td>&nbsp;</td><td><input type='submit'></td></tr>" \
                       "</table></form>" \
                       "<p>(<a href='/'>back</a>)</p>"
        view.set_html(contents)
