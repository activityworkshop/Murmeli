'''Module for testing the contact utils'''

import unittest
from murmeli import contactutils


class ContactUtilsTest(unittest.TestCase):
    '''Tests for the contact utils'''

    def check_empty_id_list(self, value):
        '''Check that an empty id list from the given value is handled properly'''
        contacts = contactutils.get_ids(value)
        self.assertFalse(contacts)
        self.assertIsNotNone(contacts)
        self.assertTrue(isinstance(contacts, set))

    def test_missing_id_list(self):
        '''Check that a missing id list is handled properly'''
        self.check_empty_id_list(None)
        self.check_empty_name_list(None)

    def test_blank_id_list(self):
        '''Check that a blank id list is handled properly'''
        self.check_empty_id_list("")
        self.check_empty_name_list("")

    def test_tooshort_id_list(self):
        '''Check that a malformed id list is handled properly'''
        self.check_empty_id_list("too short")
        self.check_empty_name_list("too short")

    def test_single_id_list(self):
        '''Check that a single id is handled properly'''
        test_id = "Cauliflower leaf"
        test_name = "Po"
        contacts = contactutils.get_ids(test_id + test_name)
        self.assertTrue(isinstance(contacts, set))
        self.assertEqual(1, len(contacts))
        self.assertTrue(test_id in contacts)

    def test_multiple_id_list(self):
        '''Check that a list of several ids is handled properly'''
        id_prefix = "Cantankerous "
        name_prefix = "Sophie"
        value = ",".join(["%s%03d%s%d" % (id_prefix, i, name_prefix, i) for i in range(5)])
        print(value)
        contacts = contactutils.get_ids(value)

        self.assertTrue(isinstance(contacts, set))
        for i in range(5):
            expected_id = "%s%03d" % (id_prefix, i)
            self.assertTrue(expected_id in contacts)
        self.assertTrue(id_prefix + "004" in contacts)
        self.assertFalse(id_prefix + "005" in contacts)


    def check_empty_name_list(self, value):
        '''Check that an empty name list from the given value is handled properly'''
        contacts = contactutils.get_ids_and_names(value)
        self.assertFalse(contacts)
        self.assertIsNotNone(contacts)
        self.assertTrue(isinstance(contacts, set))

    def test_single_idname_list(self):
        '''Check that a single id/name pair is handled properly'''
        test_id = "Cauliflower leaf"
        test_name = "Po"
        contacts = contactutils.get_ids_and_names(test_id + test_name)
        self.assertTrue(isinstance(contacts, set))
        self.assertEqual(1, len(contacts))
        found_id, found_name = contacts.pop()
        self.assertEqual(test_id, found_id)
        self.assertEqual(test_name, found_name)

    def test_multiple_idnames_list(self):
        '''Check that a list of several id/name pairs is handled properly'''
        id_prefix = "Marchforgrugyn"
        name_prefix = "Awk'ward name"
        value = ",".join(["%s%02d%s%d" % (id_prefix, i, name_prefix, i) for i in range(5)])
        print(value)
        contacts = contactutils.get_ids_and_names(value)

        self.assertTrue(isinstance(contacts, set))
        self.assertEqual(5, len(contacts))
        for i in range(5):
            expected_pair = ("%s%02d" % (id_prefix, i), "%s%d" % (name_prefix, i))
            self.assertTrue(expected_pair in contacts)


if __name__ == "__main__":
    unittest.main()
