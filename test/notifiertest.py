'''Module for testing the gui notifiers'''

import unittest
import time
from murmeli import robot
from murmeli.guinotification import GuiNotifier


class RobotNotifierTest(unittest.TestCase):
    '''Tests for the robot notifier'''

    def test_robot_notifier(self):
        '''Test what happens when a notifier is instantiated and called'''
        notifier = robot.RobotNotifier(None)
        notifier.start()
        notifier.notify_gui(0)
        notifier.notify_gui(1)
        time.sleep(1) # Just to give time to see what the LEDs are doing
        notifier.stop()


class FakeGui:
    pass

class GuiNotifierTest(unittest.TestCase):
    '''Tests for the gui notifier'''

    def test_gui_notifier(self):
        '''Test what happens when a notifier is instantiated and called'''
        notifier = GuiNotifier(None, FakeGui())
        notifier.start()
        notifier.notify_gui(0)
        notifier.notify_gui(1)
        notifier.stop()


if __name__ == "__main__":
    unittest.main()
