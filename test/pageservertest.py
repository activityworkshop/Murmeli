''''Testing of the page server'''

import unittest
from murmeli.pages import PageServer


class FakeView:
    '''Class to fake the page view, will receive the html to display'''
    def __init__(self):
        self.html = None
    def set_html(self, html):
        '''Receive the html from the server'''
        self.html = html

class FakePageSet:
    '''Class to fake a page set without templates'''
    def __init__(self):
        self.response = None
        self.domain = "watermelon"
    def serve_page(self, view, path, params):
        '''Method called by page server to deliver a page'''
        self.response = "Watermelon: Url: '%s', params: '%s'" % (path, params)
        if view:
            view.set_html(self.response)


class PageServerTest(unittest.TestCase):
    '''Tests for the basics of the page server'''

    def test_missing_pagename(self):
        '''Check that accessing a missing pagename doesn't give exception'''
        server = PageServer(None)
        fake_view = FakeView()
        server.serve_page(fake_view, "/chicken.nugget/order", [])
        self.assertIsNotNone(fake_view.html, "page served")
        self.assertTrue("fancyheader" in fake_view.html, "home page delivered")

    def test_custom_pageset_called(self):
        '''Check that an added page set is properly called'''
        server = PageServer(None)
        page_set = FakePageSet()
        server.add_page_set(page_set)
        server.serve_page(None, "/watermelon/go.php", [])
        self.assertIsNotNone(page_set.response, "page_set called")
        self.assertTrue("Watermelon" in page_set.response, "watermelon delivered")

    def test_split_domain_empty(self):
        '''Check that splitting domain and path works for empty urls'''
        for url in [None, "", " ", "/", "//", "///", "http://murmeli/", "http://murmeli//"]:
            domain, path = PageServer.get_domain_and_path(url)
            self.assertEqual(domain, "", "empty domain from %s" % repr(url))
            self.assertEqual(path, "", "empty path from %s" % repr(url))

    def test_split_path_empty(self):
        '''Check that splitting domain and path works for urls with only a domain'''
        for url in ["firework", "/firework", " firework", "/firework/",
                    "http://murmeli/firework/", " http://murmeli//firework/ "]:
            domain, path = PageServer.get_domain_and_path(url)
            self.assertEqual(domain, "firework", "matching domain from %s" % repr(url))
            self.assertEqual(path, "", "empty path from %s" % repr(url))

    def test_split_domain_and_path(self):
        '''Check that splitting domain and path works for urls with both domain and path'''
        for url in ["firework/sparkler", "/firework/sparkler", " ///firework/sparkler"]:
            domain, path = PageServer.get_domain_and_path(url)
            self.assertEqual(domain, "firework", "matching domain from %s" % repr(url))
            self.assertEqual(path, "sparkler", "matching path from %s" % repr(url))


if __name__ == "__main__":
    unittest.main()
